package com.example.pos254.pos.repositories;

import com.example.pos254.pos.models.Category;
import com.example.pos254.pos.models.OrderHeader;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderHeaderRepo extends JpaRepository<OrderHeader, Long> {
    @Query("SELECT MAX (id) as maxId From OrderHeader")
    Long GetMaxOrderHeader();

    @Query("FROM OrderHeader WHERE lower(Reference) LIKE lower(concat('%',?1,'%')) ")
    public List<OrderHeader> SearchOrderHeader(String keyword);
}
