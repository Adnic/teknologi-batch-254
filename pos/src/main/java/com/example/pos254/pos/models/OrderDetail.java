package com.example.pos254.pos.models;

import javax.persistence.*;

@Entity
@Table(name = "order_detail")
public class OrderDetail extends CommonEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long Id;

    @ManyToOne
    @JoinColumn(name = "order_header_id", insertable = false, updatable = false)
    public  OrderHeader orderHeader;

   @Column(name = "order_header_id")
    private Long OrderHeaderId;

    @ManyToOne
    @JoinColumn(name = "product_id",insertable = false,updatable = false)
    public Product product;

    @Column(name = "product_id")
    private long ProductId;

    @Column(name = "quantity")
    private int Quantity;

    @Column(name = "price")
    private float Price;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public OrderHeader getOrderHeader() {
        return orderHeader;
    }

    public void setOrderHeader(OrderHeader orderHeader) {
        this.orderHeader = orderHeader;
    }

    public Long getOrderHeaderId() {
        return OrderHeaderId;
    }

    public void setOrderHeaderId(Long orderHeaderId) {
        OrderHeaderId = orderHeaderId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public long getProductId() {
        return ProductId;
    }

    public void setProductId(long productId) {
        ProductId = productId;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int quantity) {
        Quantity = quantity;
    }

    public float getPrice() {
        return Price;
    }

    public void setPrice(float price) {
        Price = price;
    }
}
