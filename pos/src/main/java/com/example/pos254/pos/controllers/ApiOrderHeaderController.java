package com.example.pos254.pos.controllers;

import com.example.pos254.pos.models.Category;
import com.example.pos254.pos.models.OrderHeader;
import com.example.pos254.pos.repositories.OrderHeaderRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiOrderHeaderController {
    @Autowired
    private OrderHeaderRepo orderHeaderRepo;

    @GetMapping("/orderheader")
    public ResponseEntity<List<OrderHeader>> GetAllOrderHeader()
    {
        try {
            List<OrderHeader> orderHeaders = this.orderHeaderRepo.findAll();
            return new ResponseEntity<>(orderHeaders, HttpStatus.OK);
        } catch (Exception exception)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/orderheader/{id}")
    public ResponseEntity<List<OrderHeader>> GetOrderHeader(@PathVariable("id")Long id)
    {
        try {
            Optional<OrderHeader> orderHeader = this.orderHeaderRepo.findById(id);
            if (orderHeader.isPresent()) {
                ResponseEntity rest = new ResponseEntity(orderHeader, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/orderheadergetmaxid")
    public ResponseEntity<Long> GetOrderHeaderMaxId()
    {
        try {
            Long oh = this.orderHeaderRepo.GetMaxOrderHeader();
            System.out.println(oh);
            return new ResponseEntity<>(oh, HttpStatus.OK);
        }catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("searchorderheader/{keyword}")
    public ResponseEntity<List<OrderHeader>> GetOrderHeaderByName(@PathVariable ("keyword")String keyword) {
        if (keyword != null) {
            List<OrderHeader> orderheader = this.orderHeaderRepo.SearchOrderHeader(keyword);
            return new ResponseEntity<>(orderheader, HttpStatus.OK);
        } else {
            List<OrderHeader> orderheader = this.orderHeaderRepo.findAll();
            return new ResponseEntity<>(orderheader, HttpStatus.OK);
        }
    }

    @GetMapping("orderheadermapped")
    public ResponseEntity<Map<String, Object>> GetAllpage(@RequestParam(defaultValue = "0")int page,
                                                          @RequestParam(defaultValue = "5")int size)
    {
        try {
            List<OrderHeader> orderheader = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);
            Page<OrderHeader> pageTuts;
            pageTuts = orderHeaderRepo.findAll(pagingSort);
            orderheader = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("orderheader", orderheader);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/orderheader")
    public ResponseEntity<Object> SaveOrderHeader(@RequestBody OrderHeader orderheader)
    {
        String reference = "" + System.currentTimeMillis();
        orderheader.setReference(reference);
        OrderHeader orderHeaderData = this.orderHeaderRepo.save(orderheader);
        try {
            orderheader.setCreatedOn(new Date());
            orderheader.setCreatedBy("name");
            this.orderHeaderRepo.save(orderheader);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/orderheader/{id}")
    public ResponseEntity<Object> UpdateOrderHeader(@RequestBody OrderHeader orderheader, @PathVariable("id")Long id)
    {
        Optional<OrderHeader> orderHeaderData = this.orderHeaderRepo.findById(id);
        if (orderHeaderData.isPresent())
        {
            orderheader.setId(id);
            orderheader.setModifiedOn(new Date());
            orderheader.setModifiedBy("name");
            this.orderHeaderRepo.save(orderheader);
            ResponseEntity rest = new ResponseEntity<>("Update Success", HttpStatus.OK);
            return rest;
        }else {
            return ResponseEntity.notFound().build();
        }
    }

}
