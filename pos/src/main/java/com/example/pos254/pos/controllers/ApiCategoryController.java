package com.example.pos254.pos.controllers;
import com.example.pos254.pos.models.Category;
import com.example.pos254.pos.repositories.CategoryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

@RestController
@CrossOrigin ("*")
@RequestMapping ("/api")
public class ApiCategoryController {

    @Autowired
    private CategoryRepo categoryRepo;

    @GetMapping(value = "index")
    public ModelAndView index() {
        ModelAndView view = new ModelAndView("category/index");
        return view;
    }

    @GetMapping("/category") // Buat apa?
    public ResponseEntity<List<Category>> GetAllCategory() {
        try {
            List<Category> category = this.categoryRepo.findAll();
            return new ResponseEntity<>(category, HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/category")
    public ResponseEntity<Object> SaveCategory(@RequestBody Category category) {
        Category categoryData = this.categoryRepo.save(category);
        try {
        category.setCreatedOn(new Date());
        category.setCreatedBy("name");
        this.categoryRepo.save(category);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }

    }


    @GetMapping("/category/{id}")
    public ResponseEntity<List<Category>> GetCategoryById(@PathVariable("id") Long id) {
        try {
            Optional<Category> category = this.categoryRepo.findById(id);
            if (category.isPresent()) {
                ResponseEntity rest = new ResponseEntity<>(category, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("searchcategory/{keyword}")
    public ResponseEntity<List<Category>> GetCategoryByName(@PathVariable ("keyword")String keyword) {
        if (keyword != null) {
            List<Category> category = this.categoryRepo.SearchCategory(keyword);
            return new ResponseEntity<>(category, HttpStatus.OK);
        } else {
            List<Category> category = this.categoryRepo.findAll();
            return new ResponseEntity<>(category, HttpStatus.OK);
        }
    }

    @GetMapping("categorymapped")
    public ResponseEntity<Map<String, Object>> GetAllpage(@RequestParam(defaultValue = "0")int page,
                                                          @RequestParam(defaultValue = "5")int size)
    {
        try {
            List<Category> category = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);
            Page<Category> pageTuts;
            pageTuts = categoryRepo.findAll(pagingSort);
            category = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("category", category);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/category/{id}")
    public ResponseEntity<Object> UpdateCategory(@RequestBody Category category, @PathVariable("id") Long id) {
        Optional<Category> categoryData = this.categoryRepo.findById(id);
        if (categoryData.isPresent()) {
            category.setId(id);
            category.setModifiedOn(new Date());
            category.setModifiedBy("name");

            this.categoryRepo.save(category);

            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/category/{id}")
    public ResponseEntity<Object> DeleteCategory(@PathVariable("id") Long id){
        this.categoryRepo.deleteById(id);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }



}