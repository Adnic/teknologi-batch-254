package com.example.pos254.pos.controllers;


import com.example.pos254.pos.models.Product;
import com.example.pos254.pos.models.Variant;
import com.example.pos254.pos.repositories.VariantRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;


@RestController
@CrossOrigin ("*")
@RequestMapping ("/api")
public class ApiVariantController {

    @Autowired
    private VariantRepo variantRepo;
//
//    @GetMapping(value = "index")
//    public ModelAndView index(){
//        ModelAndView view = new ModelAndView("variant/index");
//        return view;
//    }

    @GetMapping("/variant")
    public ResponseEntity<List<Variant>> GetAllVariant() {
        try {
            List<Variant> variant = this.variantRepo.findAll();
            return new ResponseEntity<>(variant, HttpStatus.OK);
        } catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/variantbycategory/{id}")
    public  ResponseEntity<List<Variant>> GetAllVariantByCategory(@PathVariable("id") Long id)
    {
        try {
            List<Variant> variant = this.variantRepo.FindByCategoryId(id);
            return new ResponseEntity<>(variant, HttpStatus.OK);
        }
        catch (Exception exception)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/variant")
    public ResponseEntity<Object> SaveVariant(@RequestBody Variant variant) {
        Variant variantData = this.variantRepo.save(variant);
        try {
            variant.setCreatedOn(new Date());
            variant.setCreatedBy("name");
            this.variantRepo.save(variant);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/variant/{id}")
    public ResponseEntity<List<Variant>> GetVariantById(@PathVariable("id") Long id) {
        try {
            Optional<Variant> variant = this.variantRepo.findById(id);
            if (variant.isPresent()) {
                ResponseEntity rest = new ResponseEntity<>(variant, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception exception) {
            return new
                    ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("searchvariant/{keyword}")
    public ResponseEntity<List<Variant>> GetVariantByName(@PathVariable ("keyword")String keyword) {
        if (keyword != null) {
            List<Variant> variant = this.variantRepo.SearchVariatn(keyword);
            return new ResponseEntity<>(variant, HttpStatus.OK);
        } else {
            List<Variant> variant = this.variantRepo.findAll();
            return new ResponseEntity<>(variant, HttpStatus.OK);
        }
    }

    @GetMapping("variantmapped")
    public ResponseEntity<Map<String, Object>> GetAllpage(@RequestParam(defaultValue = "0")int page,
                                                          @RequestParam(defaultValue = "5")int size)
    {
        try {
            List<Variant> variant = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);
            Page<Variant> pageTuts;
            pageTuts = variantRepo.findAll(pagingSort);
            variant = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("variant", variant);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PutMapping("/variant/{id}")
    public ResponseEntity<Object> UpdateVariant(@RequestBody Variant variant, @PathVariable("id") Long id) {
        Optional<Variant> variantData = this.variantRepo.findById(id);
        if (variantData.isPresent()) {
            variant.setId(id);
            variant.setId(id);
            variant.setModifiedOn(new Date());
            variant.setModifiedBy("name");
            this.variantRepo.save(variant);

            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        } else {
            return ResponseEntity.notFound().build();
        }
    }



    @DeleteMapping("/variant/{id}")
    public ResponseEntity<Object> DeleteVariant(@PathVariable("id") Long id) {
        this.variantRepo.deleteById(id);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }


}
