package com.example.pos254.pos.controllers;

import com.example.pos254.pos.models.Category;
import com.example.pos254.pos.models.OrderDetail;
import com.example.pos254.pos.repositories.OrderDetailRepo;
import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiOrderDetailController {
    @Autowired
    private OrderDetailRepo orderDetailRepo;

    @GetMapping("/orderdetail")
    public ResponseEntity<List<OrderDetail>> GetAllOrderDetail()
    {
        try {
            List<OrderDetail> orderDetails = this.orderDetailRepo.findAll();
            return new ResponseEntity<>(orderDetails, HttpStatus.OK);
        } catch (Exception exception){
            return new
                    ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/orderdetailbyorder/{id}")
    public ResponseEntity<List<OrderDetail>> GetAllOrderById(@PathVariable("id")Long id)
    {
        try
        {
            List<OrderDetail> orderDetail = this.orderDetailRepo.FindByHeaderId(id);
            return new ResponseEntity<>(orderDetail,HttpStatus.OK);
        } catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }


    @PostMapping("/orderdetail")
    public ResponseEntity<Object> saveOrderDetail(@RequestBody OrderDetail orderDetail)
    {
        OrderDetail orderDetailData = this.orderDetailRepo.save(orderDetail);
        try {
            orderDetail.setCreatedOn(new Date());
            orderDetail.setCreatedBy("name");
            this.orderDetailRepo.save(orderDetail);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/orderdetail/{id}")
    public ResponseEntity<Object> updateOrderDetail(@RequestBody OrderDetail orderDetail, @PathVariable("id")Long id)
    {
        Optional<OrderDetail> orderDetailData = this.orderDetailRepo.findById(id);
        if (orderDetailData.isPresent())
        {
            orderDetail.setId(id);
            orderDetail.setModifiedOn(new Date());
            orderDetail.setModifiedBy("name");
            this.orderDetailRepo.save(orderDetail);
            ResponseEntity rest = new ResponseEntity("Update Success", HttpStatus.OK);
            return rest;
        } else {
            return ResponseEntity.notFound().build();
        }
    }


}
