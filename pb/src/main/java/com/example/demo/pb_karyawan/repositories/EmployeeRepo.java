package com.example.demo.pb_karyawan.repositories;

import com.example.demo.pb_karyawan.models.Employee;
import com.example.demo.pb_karyawan.models.Position;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EmployeeRepo extends JpaRepository<Employee, Long> {
    @Query("FROM Employee WHERE lower(EmployeeName) LIKE lower(concat('%',?1,'%')) ")
    public List<Employee> SearchEmployee(String keyword);
}
