package com.example.demo.pb_karyawan.models;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "employee")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "employee_nip")
    private String EmployeeNip;

    @Column(name = "employee_name")
    private String EmployeeName;

    @Column(name = "employee_dob")
    private Date EmployeeDob;

    @Column(name = "employee_pob")
    private String EmployeePob;

    @Column(name = "employee_nohp")
    private String EmployeeNohp;

    @Column(name = "employee_email")
    private String EmployeeEmail;

    @ManyToOne
    @JoinColumn(name = "position_id", insertable = false, updatable=false)
    public Position position;

    @Column(name = "position_name")
    private long PositionName;

    @ManyToOne
    @JoinColumn(name = "role_id", insertable = false, updatable=false)
    public Role role;

    @Column(name = "role_name", nullable = true)
    private long RoleName;

    @Column(name = "employee_agama")
    private String EmployeeAgama;

    @Column(name = "sisa_cuti")
    private long SisaCuti = 12;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmployeeNip() {
        return EmployeeNip;
    }

    public void setEmployeeNip(String employeeNip) {
        EmployeeNip = employeeNip;
    }

    public String getEmployeeName() {
        return EmployeeName;
    }

    public void setEmployeeName(String employeeName) {
        EmployeeName = employeeName;
    }

    public Date getEmployeeDob() {
        return EmployeeDob;
    }

    public void setEmployeeDob(Date employeeDob) {
        EmployeeDob = employeeDob;
    }

    public String getEmployeePob() {
        return EmployeePob;
    }

    public void setEmployeePob(String employeePob) {
        EmployeePob = employeePob;
    }

    public String getEmployeeNohp() {
        return EmployeeNohp;
    }

    public void setEmployeeNohp(String employeeNohp) {
        EmployeeNohp = employeeNohp;
    }

    public String getEmployeeEmail() {
        return EmployeeEmail;
    }

    public void setEmployeeEmail(String employeeEmail) {
        EmployeeEmail = employeeEmail;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public long getPositionName() {
        return PositionName;
    }

    public void setPositionName(long positionName) {
        PositionName = positionName;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public long getRoleName() {
        return RoleName;
    }

    public void setRoleName(long roleName) {
        RoleName = roleName;
    }

    public String getEmployeeAgama() {
        return EmployeeAgama;
    }

    public void setEmployeeAgama(String employeeAgama) {
        EmployeeAgama = employeeAgama;
    }

    public long getSisaCuti() {
        return SisaCuti;
    }

    public void setSisaCuti(long sisaCuti) {
        SisaCuti = sisaCuti;
    }
}
