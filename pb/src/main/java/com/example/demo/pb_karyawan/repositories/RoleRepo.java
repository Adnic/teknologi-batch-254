package com.example.demo.pb_karyawan.repositories;

import com.example.demo.pb_karyawan.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface RoleRepo extends JpaRepository<Role, Long> {
    @Query("FROM Role WHERE lower(RoleName) LIKE lower(concat('%',?1,'%')) ")
    public List<Role> SearchRole(String keyword);
}
