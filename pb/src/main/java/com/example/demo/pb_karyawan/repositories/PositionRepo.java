package com.example.demo.pb_karyawan.repositories;

import com.example.demo.pb_karyawan.models.Position;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PositionRepo extends JpaRepository<Position, Long>  {
    @Query("FROM Position WHERE lower(PositionName) LIKE lower(concat('%',?1,'%')) ")
    public List<Position> SearchPosition(String keyword);

}
