package com.example.demo.pb_karyawan.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/cuti")
public class CutiController {

    @Autowired
    private CutiController cutiController;

    @GetMapping(value = "index")
    public ModelAndView index()
    {
        ModelAndView view = new ModelAndView("cuti/index");
        return view;
    }
}
