package com.example.demo.pb_karyawan.repositories;

import com.example.demo.pb_karyawan.models.Cuti;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CutiRepo extends JpaRepository<Cuti, Long> {
}
