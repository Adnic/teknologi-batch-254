package com.example.demo.pb_karyawan.controller;

import com.example.demo.pb_karyawan.repositories.EmployeeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private EmployeeRepo employeeRepo;

    @GetMapping(value = "index")
    public ModelAndView index()
    {
        ModelAndView view = new ModelAndView("employee/index");
        return view;
    }
}
