package com.example.demo.pb_karyawan.controller;

import com.example.demo.pb_karyawan.models.Role;
import com.example.demo.pb_karyawan.repositories.RoleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiRoleController {

    @Autowired
    private RoleRepo roleRepo;

    @GetMapping("/role") //
    public ResponseEntity<List<Role>> GetAllrole() {
        try {
            List<Role> category = this.roleRepo.findAll();
            return new ResponseEntity<>(category, HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/role")
    public ResponseEntity<Object> Saverole(@RequestBody Role role) {
        Role roleData = this.roleRepo.save(role);
        try {
            return new ResponseEntity<>("Success", HttpStatus.OK);
        } catch(Exception exception){
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/role/{id}")
    public ResponseEntity<List<Role>> GetroleById(@PathVariable("id") Long id) {
        try {
            Optional<Role> role = this.roleRepo.findById(id);
            if (role.isPresent()) {
                ResponseEntity rest = new ResponseEntity<>(role, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("searchrole/{keyword}")
    public ResponseEntity<List<Role>> GetRoleByName(@PathVariable ("keyword")String keyword) {
        if (keyword != null) {
            List<Role> role = this.roleRepo.SearchRole(keyword);
            return new ResponseEntity<>(role, HttpStatus.OK);
        } else {
            List<Role> role = this.roleRepo.findAll();
            return new ResponseEntity<>(role, HttpStatus.OK);
        }
    }

    @GetMapping("rolemapped")
    public ResponseEntity<Map<String, Object>> GetAllpage(@RequestParam(defaultValue = "0")int page,
                                                          @RequestParam(defaultValue = "5")int size)
    {
        try {
            List<Role> role = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);
            Page<Role> pageTuts;
            pageTuts = roleRepo.findAll(pagingSort);
            role = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("role", role);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PutMapping("/role/{id}")
    public ResponseEntity<Object> Updaterole(@RequestBody Role role, @PathVariable("id") Long id) {
        Optional<Role> roleData = this.roleRepo.findById(id);
        if (roleData.isPresent()) {
            role.setId(id);
            this.roleRepo.save(role);

            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/role/{id}")
    public ResponseEntity<Object> Deleterole(@PathVariable("id") Long id){
        this.roleRepo.deleteById(id);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }
}
