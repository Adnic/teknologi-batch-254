package com.example.demo.pb_karyawan.models;

import javax.persistence.*;
import java.util.Date;
@Entity
@Table(name = "cuti")
public class Cuti {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @ManyToOne
    @JoinColumn(name = "employee_id", insertable = false, updatable = false)
    public Employee employee;

    @Column(name = "employee_Id")
    private Long EmployeeId;

    @Column(name = "jenisCuti")
    private String jenisCuti;

    @Column(name = "tanggalMulai")
    private Date tanggalMulai;

    @Column(name = "tanggalSelesai")
    private Date tanggalSelesai;

    @Column(name = "lamaCuti")
    private int lamaCuti;

    @Column(name = "alasanCuti")
    private String alasanCuti;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Long getEmployeeId() {
        return EmployeeId;
    }

    public void setEmployeeId(Long employeeId) {
        EmployeeId = employeeId;
    }

    public String getJenisCuti() {
        return jenisCuti;
    }

    public void setJenisCuti(String jenisCuti) {
        this.jenisCuti = jenisCuti;
    }

    public Date getTanggalMulai() {
        return tanggalMulai;
    }

    public void setTanggalMulai(Date tanggalMulai) {
        this.tanggalMulai = tanggalMulai;
    }

    public Date getTanggalSelesai() {
        return tanggalSelesai;
    }

    public void setTanggalSelesai(Date tanggalSelesai) {
        this.tanggalSelesai = tanggalSelesai;
    }

    public int getLamaCuti() {
        return lamaCuti;
    }

    public void setLamaCuti(int lamaCuti) {
        this.lamaCuti = lamaCuti;
    }

    public String getAlasanCuti() {
        return alasanCuti;
    }

    public void setAlasanCuti(String alasanCuti) {
        this.alasanCuti = alasanCuti;
    }
}
